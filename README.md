## Description

Repository scanner application

## Installation

```bash
# Make sure that you have installed Node.js (v14+) and NPM (v6+) on your system.
# Make sure that you have Docker installed in your system.

# Clone the repo or download and unzip it to any directory on your machine with appropriate archive manager.

# run this command from the root project folder:
$ npm install
```

## Running the app

```bash

# Create .env file on the project's root directory and fill this file with variables
# listed in:
$ env.example file
# You will need to generate personal GitHub API acces token in order to use application.
# To generate a token you should go to your GitHun profile page,
# then visit Settings -> Developer Settings -> Personal access tokens
# and generate a token which you need to provide for
$ GITHUB_AUTH_TOKEN # environment variable.

# App will launch in debug mode which is convenient for development purposes.
$ docker-compose up

# You can change the application run mode in docker-compose.yml file.
# Change this line of code to one of the available options:
$ command: npm run start:debug # Default mode

# development with watch mode
$ start:dev

# production with mode
$ start
```

## Testing the app's GET /repository/:username route

```bash
# To run the tests on host machnine be sure to install all dependency first with:
$ npm install

# To run tests from Docker container, please enter the container with application first and then run test command.
$ docker exec -it main sh

# After that you can run all tests with command:
$ npm run test
```

## API documentation

```bash
# After application start Swagger API should be available on http://localhost:3000/api
```

## Common recommendations

```bash
# There is also a Postman collection in the root project directory that can help you to test
# application after installation as well.
```
