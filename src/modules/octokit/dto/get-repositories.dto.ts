import { IsNotEmpty, IsString } from 'class-validator';

export class GetRepositoriesDto {
  @IsNotEmpty()
  @IsString()
  repositoryName: string;
  @IsNotEmpty()
  @IsString()
  ownerLogin: string;
}
