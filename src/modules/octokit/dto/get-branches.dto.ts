import { IsNotEmpty, IsString } from 'class-validator';

export class GetBranchesDto {
  @IsNotEmpty()
  @IsString()
  branchName: string;
  @IsNotEmpty()
  @IsString()
  lastCommitSha: string;
}
