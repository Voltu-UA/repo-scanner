import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { request } from '@octokit/request';
import { OctokitResponse } from '@octokit/types';

import { GetBranchesDto } from './dto/get-branches.dto';
import { GetRepositoriesDto } from './dto/get-repositories.dto';

@Injectable()
export class OctokitService {
  constructor(private configService: ConfigService) {}

  private readonly requestWithAuth = request.defaults({
    headers: {
      authorization: this.configService.get('GITHUB_AUTH_TOKEN'),
    },
  });

  async getUserRepositories(
    url: string,
    options?: Record<string, unknown>,
  ): Promise<GetRepositoriesDto[]> {
    try {
      const { data }: OctokitResponse<any> = await this.requestWithAuth(
        url,
        options,
      );

      if (!data.length) {
        throw new NotFoundException(
          `User with username ${options.username} has no active repositories`,
        );
      }

      // Reduce all fetched repositories data for further processing
      return this.reduceRepositoriesData(data);
    } catch (error) {
      if (error?.status === HttpStatus.NOT_FOUND) {
        throw new NotFoundException();
      }
      throw new Error(error?.message || JSON.stringify(error));
    }
  }

  private reduceRepositoriesData(data: any): GetRepositoriesDto[] {
    return data.reduce(
      (acc: GetRepositoriesDto[], repository: Record<string, any>) => {
        if (!repository.fork) {
          const {
            name,
            owner: { login },
          } = repository;
          acc.push({
            repositoryName: name,
            ownerLogin: login,
          });
        }
        return acc;
      },
      <GetRepositoriesDto[]>[],
    );
  }

  async getRepositoryBranches(
    url: string,
    options?: Record<string, unknown>,
  ): Promise<GetBranchesDto[]> {
    try {
      const { data }: OctokitResponse<any> = await this.requestWithAuth(url, {
        owner: options.owner,
        repo: options.repo,
      });

      // Reduce all fetched branches data for further processing
      return this.reduceBranchesData(data);
    } catch (error) {
      throw new Error(error?.message || JSON.stringify(error));
    }
  }

  private reduceBranchesData(branchesList: any): GetBranchesDto[] {
    return branchesList.reduce(
      (acc: GetBranchesDto[], branch: Record<string, any>) => {
        const {
          name,
          commit: { sha },
        } = branch;
        acc.push({
          branchName: name,
          lastCommitSha: sha,
        });
        return acc;
      },
      <GetBranchesDto[]>[],
    );
  }
}
