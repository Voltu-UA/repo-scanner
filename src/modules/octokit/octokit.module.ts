import { Module } from '@nestjs/common';

import { OctokitService } from 'src/modules/octokit/octokit.service';

@Module({
  providers: [OctokitService],
  exports: [OctokitService],
})
export class OctokitModule {}
