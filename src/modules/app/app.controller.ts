import { Controller, Get, Param } from '@nestjs/common';
import {
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { AppService } from './app.service';
import { AppErrorResponseDto } from './dto/app-error-response.dto';
import { AppResponseDto } from './dto/app-response.dto';

@ApiTags('repository-scanner')
@Controller('repository')
export class AppController {
  constructor(private readonly repositoryScannerService: AppService) {}

  @Get(':username')
  @ApiOkResponse({
    type: [AppResponseDto],
  })
  @ApiNotFoundResponse({
    type: AppErrorResponseDto,
  })
  @ApiNotAcceptableResponse({
    type: AppErrorResponseDto,
  })
  async scanRepository(
    @Param('username') username: string,
  ): Promise<AppResponseDto[]> {
    return this.repositoryScannerService.scanUserRepositories(username);
  }
}
