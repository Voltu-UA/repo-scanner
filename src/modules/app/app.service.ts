import { Injectable } from '@nestjs/common';

import { GetRepositoriesDto } from 'src/modules/octokit/dto/get-repositories.dto';
import { OctokitService } from 'src/modules/octokit/octokit.service';

import { AppResponseDto } from './dto/app-response.dto';

@Injectable()
export class AppService {
  constructor(private readonly octokitService: OctokitService) {}

  async scanUserRepositories(username: string): Promise<AppResponseDto[]> {
    const repositoriesData = await this.octokitService.getUserRepositories(
      'GET /users/{username}/repos',
      {
        username,
      },
    );

    // Merge branches for each repository sequentially
    return this.mergeResults(repositoriesData);
  }

  private async mergeResults(
    repositoriesData: GetRepositoriesDto[],
  ): Promise<AppResponseDto[]> {
    try {
      const results = await repositoriesData.reduce(
        async (previousPromise, repository) => {
          const accumulator = await previousPromise;

          const branchesList = await this.octokitService.getRepositoryBranches(
            'GET /repos/{owner}/{repo}/branches',
            {
              owner: repository.ownerLogin,
              repo: repository.repositoryName,
            },
          );

          accumulator.push({
            repositoryName: repository.repositoryName,
            ownerLogin: repository.ownerLogin,
            branchesList,
          });

          return Promise.resolve(accumulator);
        },
        Promise.resolve([]),
      );
      return results;
    } catch (error) {
      throw new Error(error?.message || JSON.stringify(error));
    }
  }
}
