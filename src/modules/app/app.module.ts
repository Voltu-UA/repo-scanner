import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { OctokitModule } from 'src/modules/octokit/octokit.module';
import { checkAcceptHeaderType } from 'src/middleware';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [OctokitModule, ConfigModule.forRoot({ isGlobal: true })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(checkAcceptHeaderType).forRoutes(AppController);
  }
}
