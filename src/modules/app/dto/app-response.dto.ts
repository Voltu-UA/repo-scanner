import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

class Branch {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  branchName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  lastCommitSha: string;
}

export class AppResponseDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  repositoryName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  ownerLogin: string;
  @ApiProperty({
    type: [Branch],
  })
  @IsArray({ each: true })
  branchesList: Branch[];
}
