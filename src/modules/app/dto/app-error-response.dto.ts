import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AppErrorResponseDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  statusCode: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  mesage: string;
}
