import { readFileSync } from 'fs';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import * as yaml from 'js-yaml';

import { AppModule } from './modules/app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  // Security
  app.use(helmet());

  // Swagger
  // !!! Commented the right way of using Swagger in Nest app. !!!
  // const config = new DocumentBuilder()
  //   .setTitle('Scanner Application API')
  //   .setDescription('API for consuming data from GitHub repositories')
  //   .setVersion('1.0')
  //   .addTag('repository-scanner')
  //   .build();
  // const document = SwaggerModule.createDocument(app, config);

  // Because there is a requiremrnt in the task to use .yaml file
  const document = yaml.load(
    readFileSync(join(__dirname, './openapi.yaml'), 'utf8'),
  );
  SwaggerModule.setup('api', app, document as OpenAPIObject);

  // App bootstrap
  const configService = app.get(ConfigService);
  await app.listen(configService.get('APP_PORT'));
}
bootstrap();
