import { NotAcceptableException } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export const checkAcceptHeaderType = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { accept } = req.headers;
  if (accept !== 'application/json') throw new NotAcceptableException();
  next();
};
