FROM node:14.15.4-alpine3.10
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app
COPY src ./src
COPY package.json ./
COPY package-lock.json ./
COPY nest-cli.json ./
COPY tsconfig.build.json ./
COPY tsconfig.json ./
RUN npm ci
RUN npm run build
CMD ["node", "dist/main"]