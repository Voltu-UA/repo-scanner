import { Test } from '@nestjs/testing';
import { OctokitService } from '../src/modules/octokit/octokit.service';
import { AppService } from '../src/modules/app/app.service';

describe('AppService (unit)', () => {
  let appService: AppService;
  let octokitService: OctokitService;

  beforeAll(async () => {
    const repositoriesDataMock = [
      {
        repositoryName: 'test-name',
        ownerLogin: 'test-owner',
      },
    ];

    const branchesDataMock = [
      {
        branchName: 'master',
        lastCommitSha: '675873c127254fbc396013a08bdc72538ddade1c',
      },
    ];

    class OctokitServiceMock {
      async getUserRepositories() {
        return repositoriesDataMock;
      }

      async getRepositoryBranches() {
        return branchesDataMock;
      }
    }

    const OctokitServiceProvider = {
      provide: OctokitService,
      useClass: OctokitServiceMock,
    };

    const app = await Test.createTestingModule({
      providers: [AppService, OctokitServiceProvider],
    }).compile();

    appService = app.get<AppService>(AppService);
    octokitService = app.get<OctokitService>(OctokitService);
  });

  it('scanUserRepositories method should return proper results data', async () => {
    const resultDataMock = [
      {
        repositoryName: 'test-name',
        ownerLogin: 'test-owner',
        branchesList: [
          {
            branchName: 'master',
            lastCommitSha: '675873c127254fbc396013a08bdc72538ddade1c',
          },
        ],
      },
    ];

    const usernameMock = 'test-owner';

    jest.spyOn(appService, 'scanUserRepositories');
    jest.spyOn(octokitService, 'getUserRepositories');

    const result = await appService.scanUserRepositories(usernameMock);
    expect(result).toStrictEqual(resultDataMock);
    expect(result).toHaveLength(1);
    expect(result[0]).toBeInstanceOf(Object);
    expect(appService.scanUserRepositories).toHaveBeenCalled();
    expect(appService.scanUserRepositories).toHaveBeenCalledTimes(1);
    expect(appService.scanUserRepositories).toHaveBeenCalledWith(usernameMock);
    expect(octokitService.getUserRepositories).toHaveBeenCalled();
    expect(octokitService.getUserRepositories).toHaveBeenCalledTimes(1);
  });
});
