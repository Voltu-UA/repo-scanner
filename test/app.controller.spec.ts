import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

import { AppModule } from 'src/modules/app/app.module';
import { AppService } from 'src/modules/app/app.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const resultDataMock = [
    {
      repositoryName: 'test-name',
      ownerLogin: 'test-owner',
      branchesList: [
        {
          branchName: 'master',
          lastCommitSha: '675873c127254fbc396013a08bdc72538ddade1c',
        },
      ],
    },
  ];
  const appService = { scanUserRepositories: () => resultDataMock };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AppService)
      .useValue(appService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET should return user repositories list`, () => {
    return request(app.getHttpServer())
      .get('/repository/test-username')
      .set('Accept', 'application/json')
      .expect(200)
      .expect(appService.scanUserRepositories());
  });

  it(`/GET should return 404 error if username was not provided`, () => {
    const errorResponse = {
      statusCode: 404,
      message: 'Cannot GET /repository',
      error: 'Not Found',
    };
    return request(app.getHttpServer())
      .get('/repository')
      .set('Accept', 'application/json')
      .expect(404)
      .expect(errorResponse);
  });

  it(`/GET should return 406 error for non application/json headers`, () => {
    const errorResponse = { statusCode: 406, message: 'Not Acceptable' };
    return request(app.getHttpServer())
      .get('/repository/test-username')
      .set('Accept', 'application/xml')
      .expect(406)
      .expect(errorResponse);
  });

  afterAll(async () => {
    await app.close();
  });
});
